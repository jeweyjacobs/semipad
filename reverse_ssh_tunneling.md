A reverse tunnel is a functionality of the ssh program that allows a connection to stay open and to allow communication in reverse. This allows from a server which is inside a NAT but can access an external server, to create an entrance through firewalls by connecting to the outside server outside and using it as a "bridge".
More easily done that explained:  

## the basic command
```ssh -R server-new-tunnel-port:localhost:local-ssh-port username@server```  
as in  
```ssh -R 2222:localhost:22 tunneler@all-syste.ms```

to which we add switch -N (do not execute any command in the first ssh connection which will open the tunnel) and -T (do not open a terminal)  
```ssh -N -T -R 2222:localhost:22 tunneler@all-syste.ms```  
This command will open port 2222 on the all-syste.ms server, and use it as a tunnel that arrives to the local port 22.  

This means we will be able to connect through all-syste.ms to the server behind the NAT. (from the command it seems we are connecting to all-syste.ms, in reality the connection goes directly to the tunneled server)  
```ssh -p 2222 username@all-syste.ms```

## bypassing firewalls
corporate firewall often forbid outgoing connections whose destination port differ from the default ports 80 and 443 used by the browser. this can be worked around by adding the additional port 443 in the ssh server settings on the machine that will be used to establish the tunnel. the ssh command will then change with the addition -p 443.
```ssh -N -T -R 2222:localhost:22 tunneler@all-syste.ms -p 443```  

## automation
to be able to run this command automatically we need to add a few switches.. to keep the connection alive, to skip the host-key checking, and skip every possible question that would make the connection process hang.. this amounts to this line:  
```ssh -N -T -o StrictHostKeyChecking=no -o UserKnownHostsFile=.userknownhostsfile -o TCPKeepAlive=yes -o ServerAliveInterval=60 -R 2222:localhost:22 tunneler@all-syste.ms```

then, we can put that line in a script that checks if the connection is on, and if it is not it starts it again.  create the file **tunnel.sh** containing:  
```	

#!/bin/sh
COMMAND="ssh -N -T -o StrictHostKeyChecking=no -o UserKnownHostsFile=.userknownhostsfile -o TCPKeepAlive=yes -o ServerAliveInterval=60 -R 2222:localhost:22 tunneler@all-syste.ms"
pgrep -f -x "$COMMAND" > /dev/null 2>&1 || $COMMAND
```

finally, we add this to a cron job so it is executed every 5 minutes..  
* Open cron:
```crontab -e```
* Add:
```*/5 * * * * /bin/sh /home/username/tunnel.sh```

## other types of tunnels: port forwarding
the very same technique can be used to create tunnels for other ports, for example for an HTTP server on port 80.. the command would work pretty similarly and look for example like:  
```ssh -R 2280:localhost:80 tunneler@all-syste.ms```
