# Semipad

Workshop and prototype with the DARC research group.

## preparations: access to the server
To be able to access to the CPT server from outside the NAT, we will use a [reverse ssh tunnel](reverse_ssh_tunneling.md).

## calibre-web semipad
<https://gitlab.com/jeweyjacobs/calibre-web-semipad>


## references

### ICT as a scholastic technique
*from [In defence of the school -- A public issue (Jan Masschelein and Maarten Simons)](https://philarchive.org/archive/MASIDO-2)*
> We must experiment with new techniques and so new working methods.  In  this  respect,  ICT  can  be  approached  as  a  scholastic technique.  Scholastic  techniques  are  not  techniques  whereby  a government or a teacher achieve results by meeting a predetermined target  or  by  producing  particular  learning  gains.  On  the  contrary,  it involves  techniques  that  enable  attention  through  the  profanation  of something (the suspension of that thing’s commonplace use) and the presenting of it in such a way that it can be shared, can arouse interest and  can  bring  about  an  experience  of  ‘being  able  to’;  of  possibility. This is also connected to scholastic methods. ICT may have a unique potential  to  create  attentiveness  (indeed,  the  screen  has  the  ability to attract our attention in an unprecedented way) and to present and unlock the world – at least when ICT is freed from the many attempts to privatise, regulate and market it. Many of these techniques are geared toward capturing attention and then redirecting it as quickly as possible toward  productive  purposes,  that  is,  toward  penetrating  the  personal 
 world to increase the size of the market. In this case, we can speak of a capitalisation of attention, with the school as an appealing accomplice in the effort to reduce the world to a resource. ICT certainly does make knowledge  and  skills  freely  available  in  an  unprecedented  way,  but the challenge is whether and how it can truly bring something to life, generate  interest,  bring  about  the  experience  of  sharing  (a  ‘common good’)  and  enable  one  to  renew  the  world.  In  this  sense,  making information,  knowledge  and  expertise  available  is  not  the  same as making something public. Screens -- just as a chalkboard  might -- have a tremendous ability to attract attention, exact concentration and gather people around something, but the challenge is to explore how they help to create a (common) presence and enable study and practice. There are plenty of new work methods to devise and test out in this regard. The dictation, so we said, can be seen as a head-on encounter with and unlocking of the world of language. Is hacking not a kind of head-on encounter  with  and  unlocking  of  the  (pre)programmed  world?  Are scholastic forms of hacking possible?
